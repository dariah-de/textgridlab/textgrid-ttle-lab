package info.textgrid.lab.ttle;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class TTLEPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public final static String NL_TAGS = "NlTags";
	public final static String TAB_TAGS = "TabTags";
	//public final static String TAGS = "Tags";
	
	public TTLEPreferencePage() {
	}

	public TTLEPreferencePage(int style) {
		super(style);
	}

	public TTLEPreferencePage(String title, int style) {
		super(title, style);
	}

	public TTLEPreferencePage(String title, ImageDescriptor image, int style) {
		super(title, image, style);
	}

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {
		addField(new StringFieldEditor(NL_TAGS, "Newline Tags", getFieldEditorParent()));
		addField(new StringFieldEditor(TAB_TAGS, "Tab Tags", getFieldEditorParent()));
		/*addField(new ListEditor(TAGS, "Tags", getFieldEditorParent()) {
			
			@Override
			protected String[] parseString(String stringList) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			protected String getNewInputObject() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			protected String createList(String[] items) {
				// TODO Auto-generated method stub
				return null;
			}
		});*/
	}

}
