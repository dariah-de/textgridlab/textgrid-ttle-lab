package info.textgrid.lab.ttle;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.EditorPart;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ttle.editors.TTLEditor;
import info.textgrid.lab.ttle.editors.TTLEditorInput;

public class TTLEController {
	public static String TTLE_TYPE = "text/ttle";
	public static String TTLE_PERSPECTIVE = "info.textgrid.lab.ttle.perspective";
	
	private static TTLEController instance = null;
	
	/**
	 * Get a singleton instance of this controller
	 * 
	 * @return instance
	 */
	public static synchronized TTLEController getInstance() {
		if (instance == null) {
			instance = new TTLEController();
		}
		return instance;
	}

	/**
	 * Private basic constructor
	 */
	private TTLEController() {
		
	}
	
	public void openPerspective() {
		try {
			PlatformUI.getWorkbench().showPerspective(TTLE_PERSPECTIVE,
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
		} catch (WorkbenchException e) {
			Activator.handleError(e);
		}		
	}
	
	public void openTTLEObject(TextGridObject object) {
		openPerspective();
		
		try {
			@SuppressWarnings("unused")
			EditorPart editor = (EditorPart) PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.openEditor(new TTLEditorInput(object), TTLEditor.EDITOR_ID);
		} catch (PartInitException e) {
			Activator.handleError(e);
		}
	}
	
	public void addObject(TextGridObject object) {
		openPerspective();
		
		IEditorPart editor = PlatformUI
			.getWorkbench()
			.getActiveWorkbenchWindow()
			.getActivePage()
			.getActiveEditor();
		
		if(editor instanceof IReusableEditor) {
			PlatformUI
				.getWorkbench()
				.getActiveWorkbenchWindow()
				.getActivePage()
				.reuseEditor((IReusableEditor) editor, new TTLEditorInput(object));
		}else {
			openTTLEObject(object); // fallback
		}
	}
}
