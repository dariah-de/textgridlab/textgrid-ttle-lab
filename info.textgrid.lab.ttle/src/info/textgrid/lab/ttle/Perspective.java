package info.textgrid.lab.ttle;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class Perspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		defineActions(layout);
    	defineLayout(layout);
	}
	
    /**
     * Defines the initial actions for a page.  
     */
    public void defineActions(IPageLayout layout) {
    }
    
    /**
     * Defines the initial layout for a page.  
     */
    public void defineLayout(IPageLayout layout) {

    	String editorArea = layout.getEditorArea();

    	IFolderLayout topLeft = 
    		layout.createFolder("topLeft", IPageLayout.LEFT, (float)0.25, editorArea);//$NON-NLS-1$
		topLeft.addView("info.textgrid.lab.navigator.view");
       	
 
//    	IFolderLayout topRight= 
//    		layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.7, editorArea);//$NON-NLS-1$
//       	topRight.addView("info.textgrid.lab.ttle.editor");

//    	layout.setEditorAreaVisible(true);
    	layout.setFixed(true);
       
    }
}
