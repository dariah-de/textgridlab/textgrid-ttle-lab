package info.textgrid.lab.ttle.editors;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.IWorkbenchPartConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.TextEditor;

import com.google.gson.Gson;

public class TTLEditor extends TextEditor implements IReusableEditor {

	/**
	 * The ID of the editor as specified by the extension.
	 */
	public static final String EDITOR_ID = "info.textgrid.lab.ttle.editors.TTLEditor";
	
	/**
	 * Url for TTLE-Backend-Server
	 */
	public static final String ConfServ_Parameter = "ttle";

	private TextGridObject object = null;
	
	private String project = null;
	private String title = null;
	
	private Browser browser = null;
	private Composite parent = null;
//	private String server = "http://localhost:8080";
	private String server = "http://ttle.ztt.fh-worms.de";
	
	private String sid = RBACSession.getInstance().getSID(true);
	
	// linux: libwebkit
	// mac: immer
	// windows: safari installieren

	/**
	 * The constructor.
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		if (input instanceof IFileEditorInput)
			object = AdapterUtils.getAdapter(((IFileEditorInput) input).getFile(), TextGridObject.class);
		else if (input instanceof TTLEditorInput)
			object = ((TTLEditorInput) input).getObject();
		
		if (null == object) {
			throw new RuntimeException("Wrong input");
		}

		ConfClient confClient;
		try {
			confClient = ConfClient.getInstance();
			String server_confserv = confClient.getValue(ConfServ_Parameter);
			if(null != server_confserv) {
				server = server_confserv;
			}
		} catch (OfflineException e) {
			showError("Error while initialization", "TTLE Backend not availabel (lab offline).");
		}

		setDocumentProvider(new TTLEditorDocumentProvider());
		setSite(site);
		setInput(input);
	}
	
	public void dispose() {
		super.dispose();
	}
	
	/**
	 * This is a callback that will allow us
	 * to create the editor and initialize it.
	 */
	public void createPartControl(Composite parent) {
		this.parent = parent;
		
		try {
			browser = new Browser(parent, SWT.WEBKIT);
		} catch (SWTError e) {
			showError("Error while intialization", "Browser cannot be initialized: " + e.getLocalizedMessage() + ".\n"
					+ "\n"
					+ "Please check if your system meet the additional requirements: \n"
					+ "Windows: Only 32Bit with an actual Apple QuickTime(TM) installation.\n"
					+ "Linux: Installed libwebkit >= 1.1.5");
			return;
		}
		
		open(object);
		
		addPropertyListener(new IPropertyListener() {
			
			@Override
			public void propertyChanged(Object source, int propId) {
				if(propId != IWorkbenchPartConstants.PROP_INPUT)
					return;
				
				TTLEditorInput input = (TTLEditorInput) TTLEditor.this.getEditorInput();
				if (input instanceof IFileEditorInput)
					object = AdapterUtils.getAdapter(((IFileEditorInput) input).getFile(), TextGridObject.class);
				else if (input instanceof TTLEditorInput)
					object = ((TTLEditorInput) input).getObject();
				
				open(object);
			}
		});
		
	}
	
	/**
	 * Open the given textGridObject with the TTLE
	 * @param textGridObject object to load
	 */
	public void open(TextGridObject textGridObject) {
		
		String uri = "";
		
		try {
			if(null == project)
				project = textGridObject.getProject();
			if(null == title)
				title = textGridObject.getTitle();
			uri = textGridObject.getURI().toString().substring(9); // remove textgrid:
		} catch (CoreException e) {
			showError("Error while load data", "Necessary data not available.");
			return;
		}

		setPartName(title);

		try {
			if(uri.startsWith("newfile:")) { // new tlo
				ObjectType metadata = textGridObject.getMetadataForReading();
				metadata.setCustom(null); // avoid loop
				String metadataJson = new Gson().toJson(metadata);
				
				browser.setUrl(server+"/TTLE/?sid="+sid+"#new|"+metadataJson+"|"+project);
			}else if(textGridObject.getContentTypeID().equals("text/ttle")) { // open tlo
				browser.setUrl(server+"/TTLE/?sid="+sid+"#ttle|"+uri+"|"+project);
			}else { // open xml and plain text (maybe within new tlo ?)
				//browser.getUrl();
				browser.setUrl(server+"/TTLE/?sid="+sid+"#xml|"+uri+"|"+project);
			}
		} catch (CoreException e) {
			showError("Error while load page", e.getLocalizedMessage());
		}
	}
	
	/**
	 * Displays the given error message as popup
	 * @param title error title
	 * @param message error description
	 */
	private void showError(String title, String message) {
		MessageBox messageBox = new MessageBox(parent.getShell(),
				SWT.ICON_ERROR | SWT.OK);
		messageBox.setMessage(message);
		messageBox.setText(title);
		messageBox.open();
	}

}
