package info.textgrid.lab.ttle.editors;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

public class TTLEditorInput implements IEditorInput {
	
	private TextGridObject tgo;
	
	public TTLEditorInput(TextGridObject object) {
		//http://www.ralfebert.de/eclipse_rcp/editors/
		this.tgo = object;
	}

	public TextGridObject getObject() {
		return tgo;
	}

	public void setObject(TextGridObject object) {
		tgo = object;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		try {
			return tgo.getTitle();
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			return "unknown";
		}
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		return "A document";
	}

}
