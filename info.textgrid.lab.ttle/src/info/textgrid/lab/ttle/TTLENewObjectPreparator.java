package info.textgrid.lab.ttle;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;

import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

public class TTLENewObjectPreparator implements INewObjectPreparator {

	private TextGridObject object;

	public void setWizard(ITextGridWizard wizard) {

	}

	public void initializeObject(TextGridObject textGridObject) {
		this.object = textGridObject;
	}

	public TextGridObject getNewTGObject() {
		return this.object;
	}

	public boolean performFinish(final TextGridObject textGridObject) {
		try {
			PlatformUI.getWorkbench().showPerspective(
					TTLEController.TTLE_PERSPECTIVE,
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());

			TTLEController.getInstance().openTTLEObject(textGridObject);

		} catch (WorkbenchException e) {
			Activator.handleError(e);
		}

		return true;
	}
}