package info.textgrid.lab.ttle.views;


import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.part.ViewPart;

public class TTLEFrontend extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "info.textgrid.lab.ttle.views.TTLEFrontend";

	Browser browser = null;
	Composite parent = null;
	String server = "http://ttle.ztt.fh-worms.de";
	
	/**
	 * The constructor.
	 */
	public TTLEFrontend() {
		// linux: libwebkit
		// mac: immer
		// windows: safari installieren
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		this.parent = parent;
		
		try {
			browser = new Browser(parent, SWT.WEBKIT);
		} catch (SWTError e) {
			showError("Error while intialization", "Browser cannot be initialized.");
		}
		
		// Create the help context id for the viewer's control
//		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "info.textgrid.lab.ttle.viewer");
//		makeActions();
//		hookContextMenu();
//		hookDoubleClickAction();
//		contributeToActionBars();
	}
	
	public void open(TextGridObject textGridObject) {
		String sid = RBACSession.getInstance().getSID(true); //sid
		String user = RBACSession.getInstance().getEPPN(); //username
		
		String project = "";
		String uri = "";
		
		try {
			project = textGridObject.getProject();
			uri = textGridObject.getURI().toString().substring(9); // remove textgrid:
		} catch (CoreException e) {
			showError("Error while load data", "Necessary data not available.");
		}
		
		try {
			if(textGridObject.getContentTypeID().equals("text/ttle")) {
				browser.setUrl(server+"/TTLE/?sid="+sid+"&user="+user+"#ttle|"+uri+"|"+project);
			}else {
				browser.setUrl(server+"/TTLE/?sid="+sid+"&user="+user+"#xml|"+uri+"|"+project);
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void showError(String title, String message) {
		MessageBox messageBox = new MessageBox(parent.getShell(),
				SWT.ICON_ERROR | SWT.OK);
		messageBox.setMessage(message);
		messageBox.setText(title);
		messageBox.open();
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
}