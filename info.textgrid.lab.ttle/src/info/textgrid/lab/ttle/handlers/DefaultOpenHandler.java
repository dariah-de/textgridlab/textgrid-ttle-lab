package info.textgrid.lab.ttle.handlers;

import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import info.textgrid.lab.ttle.Activator;
import info.textgrid.lab.ttle.TTLEController;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

public class DefaultOpenHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (!(selection instanceof IStructuredSelection)) {
			return null;
		}
		
		IStructuredSelection sel = (IStructuredSelection) selection;
		TextGridObject object = AdapterUtils.getAdapter(sel.getFirstElement(), TextGridObject.class);
		
		if (object == null) {
			return null;
		}
		
		try {
			if (TGContentType.of(TTLEController.TTLE_TYPE).equals(object.getContentType(false))) {
				TTLEController.getInstance().openTTLEObject(object);
			} else if (TGContentType.of("text/xml").equals(object.getContentType(false))
					&& TTLEController.TTLE_PERSPECTIVE.equals(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage().getPerspective().getId()))
				TTLEController.getInstance().addObject(object);
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}

		return null;
	}

}
